//
//  ScoreViewModel.swift
//  NYCSS
//
//  Created by futurehelp on 5/1/21.
//

import Foundation

struct ScoreViewModel: Decodable {
    let dbn: String
    let school_name: String
    let num_of_sat_test_takers: String
    let sat_critical_reading_avg_score: String
    let sat_math_avg_score: String
    let sat_writing_avg_score: String
    
    init(score: Score) {
        self.dbn = score.dbn
        self.school_name = score.school_name
        self.num_of_sat_test_takers = score.num_of_sat_test_takers
        self.sat_critical_reading_avg_score = score.sat_critical_reading_avg_score
        self.sat_math_avg_score = score.sat_math_avg_score
        self.sat_writing_avg_score = score.sat_writing_avg_score
    }
}
