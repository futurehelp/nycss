//
//  SchoolViewModel.swift
//  NYCSS
//
//  Created by futurehelp on 5/1/21.
//

import Foundation

struct SchoolViewModel: Decodable {
    //  (almost) all of the relevant fields for each NYC School
    //  more can always be added based on relevance to app
    
    let dbn: String
    let school_name: String
    let boro: String
    let overview_paragraph: String
    //  let school_10th_seats: Int?
    //  let academicopportunities1: String?
    //  let academicopportunities2: String?
    //  let academicopportunities3: String?
    //  let academicopportunities4: String?
    //  let ell_programs: String?
    let neighborhood: String
    //  let building_code: String?
    //  let location: String?   //  get longitude and latitude in MV, which could be used for mapping purposes via MapKit
    let phone_number: String
    let fax_number: String
    let school_email: String?
    let website: String
    //  let subway: String
    //  let grades2018: String
    let finalgrades: String
    let total_students: String    //  found this value could be nil, will have to handle in VM
    //  let extracurricular_activities: String?
    let attendance_rate: String
    //  let school_accessibility_description: Int   // probably a good field for students with disabilities
    let primary_address_line_1: String //  Street
    let city: String
    let zip: String
    let state_code: String
    let latitude: String
    let longitude: String
    
    init(school: School) {
        self.dbn = school.dbn
        self.school_name = school.school_name
        self.boro = school.boro
        self.overview_paragraph = school.overview_paragraph
        self.neighborhood = school.neighborhood
        self.phone_number = school.phone_number
        if let faxNumber = school.fax_number {
            self.fax_number = faxNumber
        }
        else {
            self.fax_number = ""
        }
        if let schoolEmail = school.school_email {
            self.school_email = schoolEmail
        }
        else {
            self.school_email = ""
        }
        self.website = school.website
        self.finalgrades = school.finalgrades
        self.total_students = school.total_students
        self.attendance_rate = school.attendance_rate
        self.primary_address_line_1 = school.primary_address_line_1
        self.city = school.city
        self.zip = school.zip
        self.state_code = school.state_code
        if let schoolLatitude = school.latitude {
            self.latitude = schoolLatitude
        }
        else {
            self.latitude = ""
        }
        if let schoolLongitude = school.longitude {
            self.longitude = schoolLongitude
        }
        else {
            self.longitude = ""
        }
    }
}
