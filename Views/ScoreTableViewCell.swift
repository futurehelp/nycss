//
//  ScoreTableViewCell.swift
//  NYCSS
//
//  Created by futurehelp on 5/2/21.
//

import Foundation
import UIKit

class ScoreTableViewCell: UITableViewCell {
    //  UI Objects
    let scoreLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "HelveticaNeue", size: 18)
        label.textColor = UIColor(hex: "E6EED6")
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.numberOfLines = 0
        label.text = "School Name"  //  default school name in case something does not autopopulate, can handle in VM
        label.alpha = 0.9
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = UIColor(hex: "343434")
        addSubview(scoreLabel)
        scoreLabel.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true
        scoreLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 20).isActive = true
        scoreLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -20).isActive = true
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

}
