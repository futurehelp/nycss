//
//  SchoolViewController.swift
//  NYCSS
//
//  Created by futurehelp on 5/2/21.
//

import UIKit

class SchoolViewController: UIViewController {
    //  class variables
    var schoolViewModel: SchoolViewModel? = nil
    var scoreViewModels: [ScoreViewModel] = []
    var showScores = true
    
    //  class UI Objects
    var activityIndicator = UIActivityIndicatorView()
    var spinner = UIActivityIndicatorView(style: .large)
    var loadingView: UIView = UIView()
    
    let schoolNameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "HelveticaNeue", size: 24)
        label.textColor = UIColor(hex: "E6EED6")
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.numberOfLines = 0
        label.text = "School Name"  //  default school name in case something does not autopopulate, can handle in VM
        label.textAlignment = .center
        label.alpha = 0.9
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let separatorLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor(hex: "659AD2")
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let scoresLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "HelveticaNeue", size: 18)
        label.textColor = UIColor(hex: "E6EED6")
        label.text = "Scores"
        label.alpha = 0.9
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let numberOfSATTakersLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "HelveticaNeue", size: 18)
        label.textColor = UIColor(hex: "E6EED6")
        label.text = "0"
        label.alpha = 0.9
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let mathLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "HelveticaNeue", size: 18)
        label.textColor = UIColor(hex: "E6EED6")
        label.text = "SAT Math Avg Score"
        label.alpha = 0.9
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let criticalReadingLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "HelveticaNeue", size: 18)
        label.textColor = UIColor(hex: "E6EED6")
        label.text = "SAT Critical Reading Avg Score"
        label.alpha = 0.9
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let writingLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "HelveticaNeue", size: 18)
        label.textColor = UIColor(hex: "E6EED6")
        label.text = "SAT Writing Avg Score"
        label.alpha = 0.9
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let websiteButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = UIColor(hex: "2F3061")
        button.setTitleColor(UIColor(hex: "ffffff"), for: .normal)
        button.setTitle("  View Website ", for: .normal)
        button.titleLabel?.font =  UIFont(name: "HelveticaNeue-Italic", size: 16)
        button.contentHorizontalAlignment = .left
        button.alpha = 0.9
        button.addTarget(self, action: #selector(websiteButtonAction), for: .touchUpInside)
        return button
    }()
    
    let overviewLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "HelveticaNeue", size: 18)
        label.textColor = UIColor(hex: "E6EED6")
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.numberOfLines = 0
        label.text = "School Overview"
        label.alpha = 0.9
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        //  Error handling for passing School reference
        if let school = schoolViewModel {
            showActivityIndicator()
            //  setupUI()
            fetchScores(dbn: school.dbn)
        }
        else {
            print("Error passing school reference.")
        }
    }
    
    func showActivityIndicator() {
        DispatchQueue.main.async {
            self.loadingView = UIView()
            self.loadingView.frame = CGRect(x: 0.0, y: 0, width: 200, height: 200)
            self.loadingView.center = self.view.center
            self.loadingView.backgroundColor = .clear
            self.loadingView.clipsToBounds = true
            self.spinner = UIActivityIndicatorView(style: .large)
            self.spinner.color = UIColor.gray
            self.spinner.frame = CGRect(x: 0.0, y: 0.0, width: 80.0, height: 80.0)
            self.spinner.center = CGPoint(x:self.loadingView.bounds.size.width / 2, y:self.loadingView.bounds.size.height / 2)
            self.loadingView.addSubview(self.spinner)
            self.view.addSubview(self.loadingView)
            self.spinner.startAnimating()
        }
    }
    
    func hideActivityIndicator() {
        DispatchQueue.main.async {
            self.spinner.stopAnimating()
            self.loadingView.removeFromSuperview()
        }
    }
    
    func setupUI() {
        view.backgroundColor = UIColor(hex: "000000")
        //  navigation title
        self.title = "NYC School Stats"
        
        view.addSubview(schoolNameLabel)
        schoolNameLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 20).isActive = true
        schoolNameLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        schoolNameLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 10).isActive = true
        schoolNameLabel.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -10).isActive = true
        schoolNameLabel.text = schoolViewModel?.school_name
        
        view.addSubview(separatorLabel)
        separatorLabel.topAnchor.constraint(equalTo: schoolNameLabel.bottomAnchor, constant: 16).isActive = true
        separatorLabel.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        separatorLabel.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
        //  check if there are scores
        view.addSubview(numberOfSATTakersLabel)
        numberOfSATTakersLabel.topAnchor.constraint(equalTo: separatorLabel.bottomAnchor, constant: 20).isActive = true
        numberOfSATTakersLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20).isActive = true
        numberOfSATTakersLabel.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20).isActive = true
        
        if showScores == true {
            view.addSubview(mathLabel)
            mathLabel.topAnchor.constraint(equalTo: numberOfSATTakersLabel.bottomAnchor, constant: 10).isActive = true
            mathLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20).isActive = true
            mathLabel.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20).isActive = true
            
            view.addSubview(criticalReadingLabel)
            criticalReadingLabel.topAnchor.constraint(equalTo: mathLabel.bottomAnchor, constant: 10).isActive = true
            criticalReadingLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20).isActive = true
            criticalReadingLabel.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20).isActive = true
            
            view.addSubview(writingLabel)
            writingLabel.topAnchor.constraint(equalTo: criticalReadingLabel.bottomAnchor, constant: 10).isActive = true
            writingLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20).isActive = true
            writingLabel.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20).isActive = true
        }
        else {
            numberOfSATTakersLabel.text = "SAT Score data is unavailable for this school."
        }
        
        view.addSubview(websiteButton)
        if showScores == true {
            websiteButton.topAnchor.constraint(equalTo: writingLabel.bottomAnchor, constant: 30).isActive = true
        }
        else {
            websiteButton.topAnchor.constraint(equalTo: numberOfSATTakersLabel.bottomAnchor, constant: 30).isActive = true
        }
        websiteButton.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        
        /*
        view.addSubview(overviewLabel)
        overviewLabel.topAnchor.constraint(equalTo: websiteButton.bottomAnchor, constant: 20).isActive = true
        overviewLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20).isActive = true
        overviewLabel.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20).isActive = true
        overviewLabel.text = schoolViewModel?.overview_paragraph.replacingOccurrences(of: "Â", with: "")
        */
    }
    
    func fetchScores(dbn: String) {
        APIService.getScores(dbn: dbn) { (scores) -> () in
            print(scores)
            //  check if there are scores
            
            //  found scores
            DispatchQueue.main.async {
                print(scores.count)
                if scores.count > 0 {
                    //  come back and make CONST variables for repeating text without having to verify an optional
                    let scoreTitle = "Number of SAT Test Takers: " + scores[0].num_of_sat_test_takers
                    self.numberOfSATTakersLabel.text = scoreTitle
                    //  really don't like using a fixed array for this, this can be scaled to handle all SAT scores and perform analysis such as lowest to highest, average, trends based on latitude/longitude locations, and an entire heap of other mathematical analysis, but for simplicity and due to time constraints, this is how this version is set up
                    self.criticalReadingLabel.text = "SAT Critical Reading Average Score: " + scores[0].sat_critical_reading_avg_score
                    self.mathLabel.text = "SAT Math Average Score: " + scores[0].sat_math_avg_score
                    self.writingLabel.text = "SAT Writing Average Score: " + scores[0].sat_writing_avg_score
                    self.scoreViewModels = scores.map({return ScoreViewModel(score: $0)})
                    self.showScores = true
                    self.hideActivityIndicator()
                }
                else {
                    print("Issue with school reference of value less than or equal to zero.")
                    self.showScores = false
                    self.hideActivityIndicator()
                }
                self.setupUI()
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @objc func websiteButtonAction(_ sender: UIButton) {
        if let website = schoolViewModel?.website {
            if let url = URL(string: "https://" + website) {
                UIApplication.shared.open(url)
            }
        }
        else {
            print("No website listed.")
        }
    }

}
