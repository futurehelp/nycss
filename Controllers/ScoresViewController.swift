//
//  ScoresViewController.swift
//  NYCSS
//
//  Created by futurehelp on 5/2/21.
//

import UIKit

class ScoresViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    //  class variables
    var scoresViewModels: [ScoreViewModel] = []  //  Will use ViewModel if necessary
    var schoolViewModel: [SchoolViewModel] = []
    var dbn = ""
    
    //  UI Objects
    var refreshControl = UIRefreshControl()
    var activityIndicator = UIActivityIndicatorView()
    var spinner = UIActivityIndicatorView(style: .large)
    var loadingView: UIView = UIView()
    
    var scoreTableView: UITableView = {
        let tableView = UITableView()
        //let tableView = UITableView.init(frame: CGRect.zero, style: .grouped)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0)
        tableView.bounces = true
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        tableView.showsVerticalScrollIndicator = false
        return tableView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        scoreTableView.delegate = self
        scoreTableView.dataSource = self
        scoreTableView.register(ScoreTableViewCell.self, forCellReuseIdentifier: "scoreCell")
        scoreTableView.backgroundColor = UIColor(hex: "000000")
        
        showActivityIndicator()
        setupUI()
        fetchScores()
    }
    
    func showActivityIndicator() {
        DispatchQueue.main.async {
            self.loadingView = UIView()
            self.loadingView.frame = CGRect(x: 0.0, y: 0, width: 200, height: 200)
            self.loadingView.center = self.view.center
            self.loadingView.backgroundColor = .clear
            self.loadingView.clipsToBounds = true
            self.spinner = UIActivityIndicatorView(style: .large)
            self.spinner.color = UIColor.gray
            self.spinner.frame = CGRect(x: 0.0, y: 0.0, width: 80.0, height: 80.0)
            self.spinner.center = CGPoint(x:self.loadingView.bounds.size.width / 2, y:self.loadingView.bounds.size.height / 2)
            self.loadingView.addSubview(self.spinner)
            self.view.addSubview(self.loadingView)
            self.spinner.startAnimating()
        }
    }
    
    func hideActivityIndicator() {
        DispatchQueue.main.async {
            self.spinner.stopAnimating()
            self.loadingView.removeFromSuperview()
        }
    }
    
    func setupUI() {
        view.backgroundColor = UIColor(hex: "333333")
        //  navigation title
        self.title = "NYC School Stats"
        
        //  UI Object layout
        view.addSubview(scoreTableView)
        scoreTableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        scoreTableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        scoreTableView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        scoreTableView.addSubview(refreshControl)
        
    }
    
    func fetchScores() {
        APIService.getAllScores() { (scores) -> () in
            print(scores.count)
            DispatchQueue.main.async {
                //  navigation title
                self.title = "NYC School Stats (\(scores.count))"
                
                //  sort schools
                let sortedSchools = scores.sorted { $0.sat_math_avg_score < $1.sat_math_avg_score }
                self.scoresViewModels.removeAll()
                self.scoresViewModels = sortedSchools.map({return ScoreViewModel(score: $0)})
                self.scoreTableView.reloadData()
                self.hideActivityIndicator()
            }
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return scoresViewModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "scoreCell", for: indexPath as IndexPath) as! ScoreTableViewCell
        print(scoresViewModels[indexPath.row].school_name)
        cell.scoreLabel.text = scoresViewModels[indexPath.row].sat_math_avg_score
        cell.backgroundColor = UIColor(hex: "333333")
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let schoolViewController = SchoolViewController()
        //  schoolViewController.schoolViewModel = schoolViewModel
        schoolViewController.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(schoolViewController, animated: false)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    @objc func refresh(sender: AnyObject) {
        // Code to refresh schoolTableView
        self.refreshControl.endRefreshing()
        fetchScores()
    }
}
