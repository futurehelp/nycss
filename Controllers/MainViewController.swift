//
//  ViewController.swift
//  NYCSS
//
//  Created by futurehelp on 5/1/21.
//

//  found coolors scheme: https://coolors.co/f7fff7-343434-2f3061-ffe66d-6ca6c1


import UIKit

class MainViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    //  class variables
    var schoolViewModels: [SchoolViewModel] = []
    var filteredSchoolViewModels: [SchoolViewModel] = []
    var schools: [School] = []
    
    //  UI Objects
    var refreshControl = UIRefreshControl()
    var activityIndicator = UIActivityIndicatorView()
    var spinner = UIActivityIndicatorView(style: .large)
    var loadingView: UIView = UIView()
    
    var schoolTableView: UITableView = {
        let tableView = UITableView.init(frame: CGRect.zero, style: .grouped)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.bounces = true
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        tableView.showsVerticalScrollIndicator = false
        return tableView
    }()
    
    let searchTextField: UITextField = {
        let textField = UITextField()
        textField.backgroundColor = UIColor(hex: "F7FFF7")
        textField.font = UIFont (name: "Helvetica", size: 15)
        textField.placeholder = "Search Schools..."
        textField.layer.cornerRadius = 9
        textField.layer.borderWidth = 0
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        return textField
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        schoolTableView.delegate = self
        schoolTableView.dataSource = self
        schoolTableView.register(SchoolTableViewCell.self, forCellReuseIdentifier: "schoolCell")
        
        showActivityIndicator()
        setupUI()
        fetchSchools()
    }
    
    func showActivityIndicator() {
        DispatchQueue.main.async {
            self.loadingView = UIView()
            self.loadingView.frame = CGRect(x: 0.0, y: 0, width: 200, height: 200)
            self.loadingView.center = self.view.center
            self.loadingView.backgroundColor = .clear
            self.loadingView.clipsToBounds = true
            self.spinner = UIActivityIndicatorView(style: .large)
            self.spinner.color = UIColor.gray
            self.spinner.frame = CGRect(x: 0.0, y: 0.0, width: 80.0, height: 80.0)
            self.spinner.center = CGPoint(x:self.loadingView.bounds.size.width / 2, y:self.loadingView.bounds.size.height / 2)
            self.loadingView.addSubview(self.spinner)
            self.view.addSubview(self.loadingView)
            self.spinner.startAnimating()
        }
    }
    
    func hideActivityIndicator() {
        DispatchQueue.main.async {
            self.spinner.stopAnimating()
            self.loadingView.removeFromSuperview()
        }
    }
    
    func setupUI() {
        view.backgroundColor = UIColor(hex: "6CA6C1")
        //  navigation title
        self.title = "NYC School Stats"
        
        //  UI Object layout
        view.addSubview(schoolTableView)
        schoolTableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: -10).isActive = true
        schoolTableView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -40).isActive = true
        schoolTableView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        schoolTableView.backgroundColor = UIColor(hex: "2F3061")
        
        if self.traitCollection.userInterfaceStyle == .dark {
            // User Interface is Dark
            searchTextField.textColor = UIColor(hex: "0000000")
            searchTextField.attributedPlaceholder = NSAttributedString(string: "Search Schools...",
                                                                       attributes: [NSAttributedString.Key.foregroundColor: UIColor(hex: "000000")])
        } else {
            // User Interface is Light
            searchTextField.textColor = UIColor(hex: "0000000")
        }
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        schoolTableView.addSubview(refreshControl)
        
    }
    
    func fetchSchools() {
        APIService.getSchools() { (schools) -> () in
            //  print(schools)
            DispatchQueue.main.async {
                //  navigation title update
                self.title = "NYC School Stats (\(schools.count))"
                //  sort schools
                let sortedSchools = schools.sorted { $0.school_name < $1.school_name }
                self.schoolViewModels.removeAll()
                self.schoolViewModels = sortedSchools.map({return SchoolViewModel(school: $0)})
                self.schoolTableView.reloadData()
                self.hideActivityIndicator()
            }
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering() {
            return filteredSchoolViewModels.count
        }
        return schoolViewModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "schoolCell", for: indexPath as IndexPath) as! SchoolTableViewCell
        //  print(schoolViewModels[indexPath.row].school_name)
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        if isFiltering() {
            cell.schoolViewModel = filteredSchoolViewModels[indexPath.row]
        }
        else {
            cell.schoolViewModel = schoolViewModels[indexPath.row]
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        dismissKeyboard()
        let schoolViewController = SchoolViewController()
        if isFiltering() {
            schoolViewController.schoolViewModel = filteredSchoolViewModels[indexPath.row]
        }
        else {
            schoolViewController.schoolViewModel = schoolViewModels[indexPath.row]
        }
        schoolViewController.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(schoolViewController, animated: false)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        view.addSubview(headerView)
        
        headerView.addSubview(searchTextField)
        searchTextField.topAnchor.constraint(equalTo: headerView.topAnchor).isActive = true
        searchTextField.leftAnchor.constraint(equalTo: headerView.leftAnchor, constant: 10).isActive = true
        searchTextField.rightAnchor.constraint(equalTo: headerView.rightAnchor, constant: -10).isActive = true
        searchTextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
        searchTextField.bottomAnchor.constraint(equalTo: headerView.bottomAnchor, constant: -20).isActive = true
        
        return headerView
    }
    
    @objc func refresh(sender: AnyObject) {
        // Code to refresh schoolTableView
        self.refreshControl.endRefreshing()
        fetchSchools()
    }
    
    //  search textfield methods
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        guard let text = textField.text else { return }
        if text == "" {
            dismissKeyboard()
        }
        else {
            filterContentForSearchText(text)
        }
    }
    
    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        filteredSchoolViewModels = schoolViewModels.filter({(schoolViewModel : SchoolViewModel) -> Bool in
            print(schoolViewModel.school_name.lowercased().contains(searchText.lowercased()))
            return schoolViewModel.school_name.lowercased().contains(searchText.lowercased())
        })
        
        schoolTableView.reloadData()
    }
    
    func isFiltering() -> Bool {
        if filteredSchoolViewModels.count > 0 {
            return true
        }
        return searchTextField.isEditing && !searchBarIsEmpty()
    }
    
    func searchBarIsEmpty() -> Bool {
        // Returns true if the text is empty or nil
        return searchTextField.text?.isEmpty ?? true
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if isFiltering() {
            dismissKeyboard()
        }
    }
}

