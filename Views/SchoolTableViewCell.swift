//
//  SchoolTableViewCell.swift
//  NYCSS
//
//  Created by futurehelp on 5/2/21.
//

import UIKit

class SchoolTableViewCell: UITableViewCell {
    //  UI Objects
    let schoolNameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "HelveticaNeue", size: 18)
        label.textColor = UIColor(hex: "FFE66D")
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.numberOfLines = 0
        label.text = "School Name"  //  default school name in case something does not autopopulate, can handle in VM
        label.alpha = 0.9
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    var schoolViewModel: SchoolViewModel? {
        didSet {
            schoolNameLabel.text = schoolViewModel?.school_name
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = UIColor(hex: "343434")
        addSubview(schoolNameLabel)
        schoolNameLabel.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true
        schoolNameLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 20).isActive = true
        schoolNameLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -20).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

}
