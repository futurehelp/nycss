//
//  School.swift
//  NYCSS
//
//  Created by futurehelp on 5/1/21.
//


import Foundation

struct School: Decodable {
    //  (almost) all of the relevant fields for each NYC School
    //  more can always be added based on relevance to app
    
    let dbn: String
    let school_name: String
    let boro: String
    let overview_paragraph: String
    //  let school_10th_seats: Int?
    //  let academicopportunities1: String?
    //  let academicopportunities2: String?
    //  let academicopportunities3: String?
    //  let academicopportunities4: String?
    //  let ell_programs: String?
    let neighborhood: String
    //  let building_code: String?
    //  let location: String?   //  get longitude and latitude in MV, which could be used for mapping purposes via MapKit
    let phone_number: String
    let fax_number: String?
    let school_email: String?
    let website: String
    //  let subway: String
    //  let grades2018: String
    let finalgrades: String
    let total_students: String   //  found this value could be nil, will have to handle in VM
    //  let extracurricular_activities: String?
    let attendance_rate: String
    //  let school_accessibility_description: Int   // probably a good field for students with disabilities
    let primary_address_line_1: String //  Street
    let city: String
    let zip: String
    let state_code: String
    let latitude: String?
    let longitude: String?
    
    init(dbn: String, school_name: String, boro: String, overview_paragraph: String, neighborhood: String, phone_number: String, fax_number: String?, school_email: String?, website: String, finalgrades: String, total_students: String, attendance_rate: String, primary_address_line_1: String, city: String, zip: String, state_code: String, latitude: String, longitude: String) {
        self.dbn = dbn
        self.school_name = school_name
        self.boro = boro
        self.overview_paragraph = overview_paragraph
        self.neighborhood = neighborhood
        self.phone_number = phone_number
        self.fax_number = fax_number
        self.school_email = school_email
        self.website = website
        self.finalgrades = finalgrades
        self.total_students = total_students
        self.attendance_rate = attendance_rate
        self.primary_address_line_1 = primary_address_line_1
        self.city = city
        self.zip = zip
        self.state_code = state_code
        self.latitude = latitude
        self.longitude = longitude
    }
}
