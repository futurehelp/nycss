//
//  Score.swift
//  NYCSS
//
//  Created by futurehelp on 5/1/21.
//

import Foundation

struct Score: Decodable {
    let dbn: String
    let school_name: String
    let num_of_sat_test_takers: String
    let sat_critical_reading_avg_score: String
    let sat_math_avg_score: String
    let sat_writing_avg_score: String
    
    init(dbn: String, school_name: String, num_of_sat_test_takers: String, sat_critical_reading_avg_score: String, sat_math_avg_score: String, sat_writing_avg_score: String) {
        self.dbn = dbn
        self.school_name = school_name
        self.num_of_sat_test_takers = num_of_sat_test_takers
        self.sat_critical_reading_avg_score = sat_critical_reading_avg_score
        self.sat_math_avg_score = sat_math_avg_score
        self.sat_writing_avg_score = sat_writing_avg_score
    }
}
