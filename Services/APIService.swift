//
//  APIService.swift
//  NYCSS
//
//  Created by futurehelp on 5/1/21.
//

//  network service to get json data and decode into models

import Foundation

struct APIService {
    static func getSchools(completionHandler: @escaping ([School]) -> ()) {
        var schools: [School] = []
        
        guard let url = URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json") else { return }
        print(url)
        URLSession.shared.dataTask(with: url) { (data, response, err) in
            guard let data = data else { return }
            do {
                schools = try JSONDecoder().decode([School].self, from: data)
            }
            catch let jsonErr {
                print("Error serializing json: ", jsonErr)
            }
            completionHandler(schools)
        }
        .resume()
    }
    
    static func getScores(dbn: String, completionHandler: @escaping ([Score]) -> ()) {
        var scores: [Score] = []
        
        guard let url = URL(string: "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn=\(dbn)") else { return }
        print(url)
        URLSession.shared.dataTask(with: url) { (data, response, err) in
            guard let data = data else { return }
            do {
                scores = try JSONDecoder().decode([Score].self, from: data)
            }
            catch let jsonErr {
                print("Error serializing json: ", jsonErr)
            }
            completionHandler(scores)
        }
        .resume()
    }
    
    static func getAllScores(completionHandler: @escaping ([Score]) -> ()) {
        var scores: [Score] = []
        
        guard let url = URL(string: "https://data.cityofnewyork.us/resource/f9bf-2cp4.json") else { return }
        print(url)
        URLSession.shared.dataTask(with: url) { (data, response, err) in
            guard let data = data else { return }
            do {
                scores = try JSONDecoder().decode([Score].self, from: data)
            }
            catch let jsonErr {
                print("Error serializing json: ", jsonErr)
            }
            completionHandler(scores)
        }
        .resume()
    }
}
